<?php
session_start();
ini_set('display_errors', true);

//Si on essaye d'accéder au dashboard sans être connecté
if (isset($_SESSION['user']['name']) && $_SESSION['type'] == 2 ):?>   

<?php else: header("Location: index.php");?>

<?php endif; ?>

<?php
$var = 0;
//On initialise la variable de vérification

if (isset($_POST['title']) && isset($_POST['description']) && isset($_POST['date_start']) && isset($_POST['date_end']) && isset($_POST['price'])) 
    {
        $str = $_POST['title'];
        $str = strlen($str);

        $strd = $_POST['description'];
        $strd = strlen($strd);

        $strds = $_POST['date_start'];
        $strds = strlen($strds);

        $strde = $_POST['date_end'];
        $strde = strlen($strde);

        $strp = $_POST['price'];
        $strp = strlen($strp);

        //on récupère la taille de données entrées dans le formulaire

        if ($str < 4)
        {
            $var = 1;
        }

        else
        {
                if ($strd < 4)
                {
                    $var = 2;
                }

                else
                {
                        if ($strds < 10)
                        {
                            $var = 3;
                        }

                        else
                        {

                                if ($strde < 10)
                                {
                                    $var = 4;
                                }

                                else
                                {

                                        if ($strp < 1)
                                        {
                                            $var = 5;
                                        }
                                                    
                                                else
                                                {
                                                    include "classes/bungalow_register.class.php";
                                                    
                                                    $new_member = new Bungalow;

                                                    $new_member->veriform();
                                                    
                                                    $countbung = $new_member->getCount();
                                                    //on récupère ce que renvoie la requête

                                                    if ($countbung == 1) 
                                                    {
                                                        $var = 6;
                                                        //si un bungalow a déjà le même nom
                                                    }

                                                    else
                                                    {
                                                        $var = 7;
                                                        $new_member->register();
                                                        //sinon on enregistre le bungalow
                                                    }   
                                                }                                        
                                }
                        }
                }
        }
}

include "menu.php";

?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Tableau de bord</title>
    </head>


    <body>

<div class="container-fluid">

        <div class="row-fluid">

            <div class="span8 offset2"> 

                <!--  Formulaire d'enregistrement  -->
                <form method="post" action="" id="formBung">

                    <fieldset>

                    <legend style="width:500px">Enregistrer un nouveau bungalow:</legend>

                        <h5>Titre:</h5>

                        <p><input type="text" name="title" autocomplete="off" placeholder="Titre de votre annonce" /></p>

                        </br>     
                        
                        <h5>Description:</h5> 

                        <p><textarea name="description" placeholder="Entrez une description" style="width: 500px"></textarea></p>

                        </br>

                        <h5>Disponibilité:</h5> 

                        <p> Du: <input type="text" name="date_start" autocomplete="off" placeholder="yyyy/mm/dd" style="width: 100px"/> Au: <input type="text" name="date_end" autocomplete="off" placeholder="yyyy/mm/dd" style="width: 100px"/></p>

                        </br>

                        <h5>Prix:</h5>

                        <p><input type="text" name="price" autocomplete="off" placeholder="Prix" style="width: 30px"/></p>

                        <p><button type="submit" class="btn btn-success pull-left">Soumettre </button> </p>

                    </fieldset>
                </form>

                <!--  séquence d'affichage des massages d'erreur avec bootstrap  -->

                <?php if ($var == 1): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Le titre entré est trop court</div>
                
                <?php elseif ($var == 2): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Veuillez entrer une description</div>
                
                <?php elseif ($var == 3): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Veuillez entrer une date de début de disponibilité valable</div>
                
                <?php elseif ($var == 4): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Veuillez entrer une date de fin de disponibilité valable</div>

                <?php elseif ($var == 5): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Veuillez entrer un prix correct</div>

                <?php elseif ($var == 6): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Le titre entré est déjà utilisé pour une autre annonce</div>

                <?php elseif ($var == 7): ?>
                  
                    <div class="alert alert-success">
                    <h4 class="alert-heading">Félicitation</h4>
                    Votre Annonce a bien été créé</div>
   
                <?php endif; ?>

            </div>

        </div>

</div>   

    </body>
</html> 
