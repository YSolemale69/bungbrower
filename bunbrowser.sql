-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 30 Septembre 2013 à 23:55
-- Version du serveur: 5.5.29
-- Version de PHP: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données: `bunbrowser`
--

-- --------------------------------------------------------

--
-- Structure de la table `bungalow`
--

CREATE TABLE `bungalow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `price` int(255) NOT NULL,
  `disponibility` int(2) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='table des bungalow' AUTO_INCREMENT=15 ;

--
-- Contenu de la table `bungalow`
--

INSERT INTO `bungalow` (`id`, `name`, `description`, `date_start`, `date_end`, `price`, `disponibility`, `user_id`) VALUES
(1, 'bungalowTest', 'c''est une belle boite', '2013-09-01', '2013-09-02', 1, 0, 0),
(2, 'bungalow test', 'test ajout 1', '2013-10-01', '2013-10-20', 2, 1, 0),
(3, 'Test2 pouf', 'Ca se trouve dans la chatte Ã  ta maman', '2013-10-01', '2013-11-01', 9999, 1, 0),
(4, 'Charmante villa', 'C''est une charmante petite villa en bord de mer, l''atmosphÃ¨re y est assez tranquille, on peut se toucher la nouille en paix et sans Ãªtre dÃ©rangÃ©.\nLe voisinage est trÃ¨s sympathique, et viendra mÃªme de temps Ã  autre vous aider Ã  vous astiquer la no', '2013-11-02', '2013-11-12', 50, 1, 0),
(6, 'TEUb', 'cibz zicezb iuebczce zieue zieuze zeze zecez zec c ve verve evree vevrev errevre ever ervev', '2103-12-12', '0000-00-00', 20, 1, 0),
(11, 'oiczeoez', 'cizoeceuzn izeuezn', '0982-12-12', '3211-12-12', 982, 1, 7),
(13, 'icuzi', 'uzce uizeb iuzebcu bicubezeiz', '0000-00-00', '1201-12-12', 972, 1, 8),
(14, 'Bungalow tits', 'pour paf pif bite pour  gour chatte oize pojg zoei une pouf salope poif suce ma bite', '0000-00-00', '0000-00-00', 7890, 1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE `commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext NOT NULL,
  `bungalow_id` varchar(11) NOT NULL,
  `validity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `commentaires`
--

INSERT INTO `commentaires` (`id`, `message`, `bungalow_id`, `validity`) VALUES
(1, 'testc!uhz czo yep', '1', 0),
(2, 'testc!uhz czo yep', '1', 0),
(3, 'plopplopo opop', '3', 0),
(4, 'ffzfzfzÃ©Ã©d"Ã© Ã©Ã©', '1', 0),
(5, 'ytzecvzt zcezu \r\n', '1', 0),
(6, 'zoicnez test\r\nczczz', '1', 0),
(7, 'ecbzz cbzeuye z cezbz', '1', 0),
(8, 'rfze zfzez zefezze efzfzez', '1', 0),
(9, 'test tes test plop', '1', 0),
(10, 'pif iuez iuze zieu zcioe', '11', 0),
(11, 'cezcjez e z ed dz dze ', '11', 0),
(12, 'izcbz ciuzen cizz', '14', 0),
(13, 'zeidun ze i ziue iuze ziue ', '14', 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Type` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `phone` int(15) NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `Type`, `name`, `lastname`, `address`, `phone`, `password`) VALUES
(1, 'test1', 0, 'testeur', 'pro', 'tro maco rue de la brouche vière', 690118218, 'supinfo'),
(2, 'qrgqzrgqerh', 0, 'ggegqerg', 'qregqrgqz', 'eqrhsrtnhsry', 123456789, 'cf0b776677f282e5177f17592232b33c52153709'),
(3, 'demandeatamaman', 0, 'yann', 'tromaco', 'fbgjelriugaerognaelrjnglaer', 690123456, 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4'),
(4, 'pseudo', 0, 'fisteur', 'profesionelle', 'dans le lit de ta maman', 690118218, 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4'),
(5, 'senseyk', 0, 'yann', 'FRANCOIS-JULIEN', 'dans le lit de ta maman', 690118218, 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4'),
(6, 'testkibi', 1, 'test', 'test plouf', '10 rue de la sale pouf', 988766554, 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4'),
(7, 'Kibi', 2, 'Yannis', 'SolÃ©malÃ©', '10 rue de la salope', 690123456, 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4'),
(8, 'testkibi1', 2, 'iub', 'iubo', 'icuezbcebiebeubcueez', 987658776, 'e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4');
