<?php
session_start();

//Si on essaye d'accéder au dashboard sans être connecté 
if (isset($_SESSION['user']['name'])):?>   

<?php else: header("Location: index.php");?>

<?php endif; ?>

<?php
$var = 0;
//On initialise la variable de vérification

include "classes/commentaire.class.php";         
$new_member = new Commentaire;

$name = $_GET['name'];
$description = $_GET['description'];
$date_start = $_GET['date_start'];
$date_end = $_GET['date_end'];
$price = $_GET['price'];
$bungalow_id = $_GET['id'];
$type = $_GET['type'];

if (isset($_POST['commentaire'])) 
    {
        $com = $_POST['commentaire'];
        $str = strlen($com);
        //on récupère la taille de données entrées dans le formulaire

        if ($str < 10)
        {
            $var = 1;
        }

        else
        {   
            $new_member->register();

            $var = 2;
        }
    }

include "menu.php";

?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Commentaires</title>
    </head>


    <body>

<!--  Affichage des infos de l'annonce  -->
<div class="container-fluid">

        <div class="row-fluid">

            <div class="span6 offset3" style="background-color: Gold;">

            <p> <h1> <?php echo $name;?> </h1> </p> 
            <br>
            <br>
            <p> <h5> <?php echo $description;?> </h5> </p>
            <br>
            <p> <h5> Disponible du : <?php echo $date_start;?> au <?php echo $date_end;?> </h5> </p> 
            <br>
            <p> <h5> <strong> Prix :<?php echo $price;?>€/nuit</strong> </h5> </p>

            </div>

        </div>

<div class="row-fluid">

            <div class="span8 offset2">

                <!--  Formulaire d'enregistrement  -->
                <form method="post" action="" id="formBung">

                    <fieldset>

                    <legend style="width:500px"><h3>Laisser un Commentaire : </h3></legend>
                        
                        <p><textarea name="commentaire" placeholder="Entrez votre commentaire" style="width: 600px"></textarea></p>

                        <p><button type="submit" class="btn btn-success pull-left">Soumettre </button> </p>

                    </fieldset>
                </form>


                <!--  séquence d'affichage des massages d'erreur avec bootstrap  -->

                <?php if ($var == 1): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Votre commentaire est trop court</div>
                
                <?php elseif ($var == 2): ?>
                                    
                    <div class="alert alert-success">
                    <h4 class="alert-heading">Félicitation</h4>
                    Votre commentaire a bien été enregistré</div>
   
                <?php endif; ?>

            </div>

        </div>

<div class="row-fluid">

    <div class="span8 offset2">
        <h3>Commentaire : </h3>

        <?php if (isset($_GET['type']) == 2): ?>
            
                <?php $new_member->afficheListeProprio(); ?>
            

        <?php elseif (isset($_GET['type']) != 2): ?>
              
                <?php $new_member->afficheListe(); ?>

        <?php endif; ?>
        
    </div>
    
</div>


</div>   

    </body>
</html> 
