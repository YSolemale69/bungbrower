<?php
session_start();
ini_set('display_errors', true);

if (isset($_SESSION['user']['name'])):?>   

<?php else: header("Location: index.php");?>

<?php endif; ?>

<!DOCTYPE html >
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Suppression</title>
    </head>


    <body>  

        <?php include"menu.php"; ?>

        <div id="main">

            <div class="container-fluid"> 

                <div class="row-fluid">
                    <div class="span12">
                
                        <?php include "classes/manager.class.php";

                        $new_manager1 = new Manager;

                        $new_manager1->remove();?> 

                    </div>

                </div>
            </div>
        </div>      
    </body>
</html>