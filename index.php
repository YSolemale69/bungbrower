<?php 
session_start();
ini_set('display_errors', true);
include "menu.php";

if (isset($_SESSION['user']['name'])):?>

<?php $_var = 0;?>

<?php else: ?>

<?php $_var = 1; ?>

<?php endif; ?>


<!DOCTYPE html >
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Bungbrowser - Accueil</title>
    </head>
 
    <body>

        <div class="container-fluid">

          <div class="row-fluid">

              <?php if ($_var == 0): ?>

                  <?php include ("classes/index.class.php");

                  echo"<div class='page-header'><h2 style='text-align: center'>Les dernières annonces</h2></div>";

                  $new_login = new Index;

                  $new_login->tableau(); ?>

              <?php else: echo "<div class='page-header'><h1 style='text-align: center'>Bienvenue sur BungBrowser</h1></div>";?>

              <?php endif; ?>
              
          </div>

          <div class="row-fluid">

            <div class="span6 offset3">
              
              <h4>
               Ce site permet la mise en location de bungalows,</br>
               et permet aussi la réservation en ligne,</br>
               pour plus d'informations veuillez vous référer à la documentation.</br>
              </h4>

            </div>

          </div>

          <div class="row-fluid">
    
            <div class="span5 offset6">
              Bungbrowser - Supinfo Project - Yannis Solémalé
            </div>

          </div>

        </div>

    </body>
</html> 
