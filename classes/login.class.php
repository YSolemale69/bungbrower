<?php
class Login
{
    private $_username;
    private $_passwd;
    private $_id;
    private $_type;
    private $_bdd;

    public $resultat;
    public $sizename;

    public function __construct()
    {
        include "connexion.php";

        $this->_username = $_POST['username'];
        $this->_passwd = $_POST['password'];
        $this->_id = NULL;
        $this->_type = NULL;
        $this->resultat = NULL;
        $this->sizename = NULL;
        //on récupère le username et le password, on met les autres attributs à null
    }

    public function preparation()
    {
        $name = $this->_username;
        $pass = $this->_passwd;
        $type = $this->_type;
        $pass_hache = sha1($pass);
        $this->_passwd = $pass_hache;
        $bdd = $this->_bdd;
        //on utilise des variables intermédaires

        $req = $bdd->prepare('SELECT Type FROM user WHERE username = ? AND password = ?');
        $req->execute(array(
        $name,
        $pass_hache));
        //on vérifie le username et le mdp dans la database

        $this->_type = $req->fetch();
        //on compte le nombre de réponses

        $req = $bdd->prepare('SELECT id FROM user WHERE username = ? AND password = ?');
        $req->execute(array(
        $name,
        $pass_hache));
        //on vérifie le username et le mdp dans la database

        $this->_id = $req->fetch();
        //on compte le nombre de réponses
        
        $this->resultat = $req->rowCount();

        if ($this->resultat == 1)
        {
            $this->_id = $this->_id['id'];
            $this->_type = $this->_type['Type'];
            $this->_username = $_POST['username'];
            //les attributs prennents les valeurs retournées par la requêtes et par le formulaire
            //si le résultat est bon
        }

        return $this->_username;
        return $this->_id;
    }  

    public function verification()
    {
        if ($this->resultat == 0)
        //si la requête ne renvoie aucun résultat, l'username ou le password n'existe pas dans la database
        {
            $_SESSION['var'] = 0;
        }

        else
        {
            $this->login();
        }
    }    

    public function login()
    {      
        $_SESSION['verif'] = 1;
        $_SESSION['user_id'] = $this->_id;
        $_SESSION['username'] = $this->_username;
        $_SESSION['type'] = $this->_type;

        $_SESSION['user']['name'] =$_SESSION['username'];
        $_SESSION['user']['id'] =$_SESSION['user_id'];
        //on assigne une valeur à chaque variable de session

        header("Location: manager.php");
    }
}
?>