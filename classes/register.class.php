<?php
class Register
{
    private $_username;
    private $_name;
    private $_lastname;
    private $_address;
    private $_phone;
    private $_passwd;
    private $_type;
    private $_bdd;

    public $countusername;

    public function __construct()
    {
        include "connexion.php";

        $this->_username = $_POST['username'];
        $this->_name = $_POST['name'];
        $this->_lastname = $_POST['lastname'];
        $this->_address = $_POST['address'];
        $this->_phone = $_POST['phone'];
        $this->_passwd = $_POST['password'];
        $this->_type = $_POST['type'];
        $this->countusername = NULL;
        //on récupère les infos du formulaire
    }

    public function veriform()
    {
        $login = $this->_username;
        $bdd = $this->_bdd;
        //variables intermédiaires

        $query = $bdd->prepare('SELECT id FROM user WHERE username = ?');
        $query -> execute( array( $login ));
        $this->countusername = $query->rowCount();
        //on regarde si le name existe déjà dans la database
    }

    public function register()
    {      
        $pseudo = $this->_username;
        $name = $this->_name;
        $lastname = $this->_lastname;
        $address = $this->_address;
        $phone = $this->_phone;
        $type = $this->_type;
        $pass_hache = sha1($this->_passwd);
        // Hachage du mot de passe
        $bdd = $this->_bdd;
        //variables intermédiaires

        $req = $bdd -> prepare ('INSERT INTO user(username, Type, name, lastname, address, phone, password) VALUES(?, ?, ?, ?, ?, ?, ?)');
        $req -> execute (array(
        $pseudo,
        $type, 
        $name,
        $lastname,
        $address,
        $phone,
        $pass_hache));
        // Insertion dans la database des données
    }

    public function getCount()
    {
        return $this->countusername;
        //on retourne le nombre de résultats renvoyé par la requête
    }
}
?>