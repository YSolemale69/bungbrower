<?php
class Bungalow
{
    private $_title;
    private $_description;
    private $_date_start;
    private $_date_end;
    private $_price;
    private $_disponibility;
    private $_user_id;
    private $_bdd;

    public $countname;

    public function __construct()
    {
        include "connexion.php";

        $this->_title = $_GET['name'];
        $this->_description = $_GET['description'];
        $this->_date_start = $_GET['date_start'];
        $this->_date_end = $_GET['date_end'];
        $this->_price = $_GET['price'];
        $this->_user_id = $_SESSION['user']['id'];
        $this->_disponibility = "1";
        //on récupère les infos du formulaire
    }

    public function veriform()
    {
        $name = $this->_title;
        $bdd = $this->_bdd;
        //variables intermédiaires

        $query = $bdd->prepare('SELECT id FROM bungalow WHERE name = ?');
        $query -> execute( array( $name ));
        $this->countname = $query->rowCount();
        //on regarde si le name existe déjà dans la database
    }

    public function register()
    {      
        $title = $this->_title;
        $description = $this->_description;
        $date_start = $this->_date_start;
        $date_end = $this->_date_end;
        $price = $this->_price;
        $id = $this->_user_id;
        $disponibility = $this->_disponibility;

        $bdd = $this->_bdd;
        //variables intermédiaires

        $req = $bdd -> prepare ('INSERT INTO bungalow(description, date_start, date_end, price, disponibility, user_id) VALUES(?, ?, ?, ?, ?, ?) WHERE name = ?');
        $req -> execute (array(
        $description,
        $date_start,
        $date_end,
        $price,
        $disponibility,
        $id,
        $title));
        // Insertion dans la database des données
    }

    public function getCount()
    {
        return $this->countname;
        //retourne le nombre de résultats renvoyés par la database
    }
}
?>