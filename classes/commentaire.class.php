<?php
class Commentaire
{
    private $_commentaire;
    private $_bungalow_id;
    private $_bdd;

    public $countname;

    public function __construct()
    {
        include "connexion.php";

        $this->_commentaire = $_POST['commentaire'];
        $this->_bungalow_id = $_GET['id'];
        //on récupère les infos du formulaire
    }


    public function register()
    {      
        $com = $this->_commentaire;        
        $id = $this->_bungalow_id;
        $val = 0;

        $bdd = $this->_bdd;
        //variables intermédiaires

        $req = $bdd -> prepare ('INSERT INTO commentaires(message, bungalow_id, validity) VALUES(?, ?, ?)');
        $req -> execute (array(
        $com,
        $id,
        $val));
        // Insertion dans la database des données
    }

    public function afficheListe()
    {
        $bdd = $this->_bdd;
        $id = $this->_bungalow_id;
        $req1 = $bdd->prepare('SELECT validity, message FROM commentaires WHERE bungalow_id = ?'); 
        $req1 -> execute (array(
        $id));
        //on sélectionne les annonces qui correspondent à l'id de l'utilisateur dont la session est en cours


            while($data = $req1->fetch(PDO::FETCH_ASSOC))
            // on fait une boucle qui va faire un tour pour chaque annonce  
            {   
                // on affiche les informations de l'url que la boucle parcours
                if ($data['validity'] == 1)
                {   
                    echo' 
                    <div class="row">
                        <h4>> '.$data['message'].'</h4> <br>
                    </div>';
                }
            }
        
        echo'</TABLE>';
    }

    public function afficheListeProprio()
    {
        $bdd = $this->_bdd;
        $id = $this->_bungalow_id;
        $req1 = $bdd->prepare('SELECT id, validity, message FROM commentaires WHERE bungalow_id = ?'); 
        $req1 -> execute (array(
        $id));
        //on sélectionne les annonces qui correspondent à l'id de l'utilisateur dont la session est en cours


            while($data = $req1->fetch(PDO::FETCH_ASSOC))
            // on fait une boucle qui va faire un tour pour chaque annonce  
            {   
                // on affiche les informations de l'url que la boucle parcours 
                if ($data['validity'] == 1)
                {
                    echo' 
                    <div class="row">
                        <h4>> '.$data['message'].'</h4> <br>
                    </div>';
                }

                else
                {
                    echo' 
                    <div class="row">
                        <h4>> '.$data['message'].'</h4> <br>
                        <a class="lien3" href="validation_commentaire.php?test=1&supp='.$data['id'].'"> Valider </a> &nbsp
                        <a class="lien3" href="validation_commentaire.php?test=0&supp='.$data['id'].'"> Supprimer </a>
                    </div>';
                }
            }
        
        echo'</TABLE>';
    }

    public function Validate()
    {
        $bdd = $this->_bdd;
        $supp = $_GET['supp'];
        //on récupère le nom de l'url à supprimer via l'envoie par url de la page précédente

        $query = $bdd->prepare('UPDATE commentaires SET validity = "1" WHERE id = ?');
        $query -> execute( array( $supp ));
        //on supprime l'url de la database

        echo 'Ce commentaire a bien été validé </br> <a class="lien2" href="manager.php">retour</a>';
    }

    public function Delete()
    {
        $bdd = $this->_bdd;
        $supp = $_GET['supp'];
        //on récupère le nom de l'url à supprimer via l'envoie par url de la page précédente

        $query = $bdd->prepare('DELETE FROM commentaires WHERE id = ?  LIMIT 1');
        $query -> execute( array( $supp ));
        //on supprime l'url de la database

        echo 'Ce commentaire a bien été supprimée </br> <a class="lien2" href="manager.php">retour</a>';
    }

}
?>