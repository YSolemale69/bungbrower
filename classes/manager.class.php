<?php
class Manager
{
    private $_id;
    private $_bdd;

    public function __construct()
    {
        include "connexion.php";

        $this->_id = $_SESSION['user']['id'];
    }
 
    public function afficheListe()
    {
        $bdd = $this->_bdd;
        $id = $this->_id;
        $type = 2;
        $req1 = $bdd->prepare('SELECT id, name, description, date_start, date_end, price FROM bungalow WHERE user_id = ?'); 
        $req1 -> execute (array(
        $id));
        //on sélectionne les annonces qui correspondent à l'id de l'utilisateur dont la session est en cours


            while($data = $req1->fetch(PDO::FETCH_ASSOC))
            // on fait une boucle qui va faire un tour pour chaque annonce  
            {   
                // on affiche les informations de l'annonce que la boucle parcours   
                echo' 
                <div class="span3"> 
                    <h3>'.$data['name'].'</h3><hr> 
                    <p><h5>'.$data['description'].'</h5></p><br>
                    <p>Disponible du: '.$data['date_start'].' au '.$data['date_end'].' </p><br>
                    <p><strong>'.$data['price'].'€/nuit</strong></p>
                    <p><div class="pull-right"> 
                    <a class="lien3" href="commentaire.php?name='.$data['name'].'&description='.$data['description'].'&date_start='.$data['date_start'].'&date_end='.$data['date_end'].'&price='.$data['price'].'&id='.$data['id'].'&type='.$type.'">Commentaires</a> &nbsp
                    <a class="lien3" href="remove.php?supp='.$data['name'].'"> Supprimer </a> &nbsp 
                    <a class="lien3" href="change.php?name='.$data['name'].'&description='.$data['description'].'&date_start='.$data['date_start'].'&date_end='.$data['date_end'].'&price='.$data['price'].'">Modifier</a>
                    </div></p>
                </div>';
            }
        
        echo'</TABLE>';
    }


    public function remove()
    {
        $bdd = $this->_bdd;
        $supp = $_GET['supp'];
        //on récupère le nom de l'url à supprimer via l'envoie par url de la page précédente

        $query = $bdd->prepare('DELETE FROM `bunbrowser`.`bungalow` WHERE `bungalow`.`name` = ?  LIMIT 1');
        $query -> execute( array( $supp ));
        //on supprime l'url de la database

        echo 'Votre annonce a bien été supprimée </br> <a class="lien2" href="manager.php">retour</a>';
    }
}
?>