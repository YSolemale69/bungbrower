<?php
class Search
{
    private $_search;
    private $_bdd;

    public function __construct()
    {
        include "connexion.php";

        $this->_search = $_POST['search'];
    }

    public function afficheSearch()
    {
        $search = $this->_search;
        $compteur = 0;

        $bdd = $this->_bdd;
        $req1 = $bdd->prepare("SELECT id, name, description, date_start, date_end, price FROM bungalow WHERE name like '%$search%' OR description like '%$search%'"); 
        $req1 -> execute ();
        //on sélectionne les infos de l'annonce

            while(($data = $req1->fetch(PDO::FETCH_ASSOC)) && ($compteur < 8))
            // on fait une boucle qui va faire un tour pour chaque annonce et qui va renvoyer 8 résultats max  
            {   
                $compteur ++;
                // on affiche les informations de l'annonce que la boucle parcours   
                echo' 
                <div class="span3"> 
                    <h3>'.$data['name'].'</h3><hr> 
                    <p><h5>'.$data['description'].'</h5></p><br>
                    <p>Disponible du: '.$data['date_start'].' au '.$data['date_end'].' </p><br>
                    <p><strong>'.$data['price'].'€/nuit</strong></p>
                    <p><a class="lien3" href="commentaire.php?name='.$data['name'].'&description='.$data['description'].'&date_start='.$data['date_start'].'&date_end='.$data['date_end'].'&price='.$data['price'].'&id='.$data['id'].'">Commentaires</a></p>
                </div>';
            }
        
        echo'</TABLE>';
    }
}
?>