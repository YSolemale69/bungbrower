<?php
session_start();
ini_set('display_errors', true);

//Si on essaye d'accéder au dashboard sans être connecté 
if (isset($_SESSION['user']['name']) && $_SESSION['type'] == 2 ):?>   

<?php else: header("Location: index.php");?>

<?php endif; ?>

<?php include "menu.php";?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Gestion des bungalows</title>
    </head>


    <body>

<div class="container-fluid">

    <div class="page-header"><h2 style='text-align: center'>Mes bugalows</h2></div>

        <div class="row-fluid">

              <?php include "classes/manager.class.php";

                $new_manager = new Manager;

                $new_manager->afficheListe(); ?>

        </div>

</div>   

    </body>
</html> 
