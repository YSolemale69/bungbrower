<?php

$var = 0;
//On initialise la variable de vérification

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['confirmation'])) 
    {
        $str = $_POST['username'];
        $str = strlen($str);

        $strp = $_POST['password'];
        $strp = strlen($strp);

        $strphone = $_POST['phone'];
        $strphone = strlen($strphone);

        $stra = $_POST['address'];
        $stra = strlen($stra);

        //on récupère la taille de l'username du password du phone number et de l'adresse

        if ($str < 4)
        {
            $var = 1;
        }

        else
        {
                if ($strp < 4)
                {
                    $var = 2;
                }

                else
                {
                        if ($_POST['password'] != $_POST['confirmation'])
                        {
                            $var = 3;
                        }

                        else
                        {

                                if ($strphone < 10)
                                {
                                    $var = 5;
                                }

                                else
                                {

                                        if ($stra < 10)
                                        {
                                            $var = 6;
                                        }
                                                    
                                                else
                                                {
                                                    include "classes/register.class.php";
                                                    
                                                    $new_member = new Register;

                                                    $new_member->veriform();
                                                    
                                                    $countuser = $new_member->getCount();
                                                    //on récupère la réponse que renvoie la requête

                                                    if ($countuser == 1) 
                                                    {
                                                        $var = 7;
                                                        //si l'user existe déjà
                                                    }

                                                    else
                                                    {
                                                        $var = 8;
                                                        $new_member->register();
                                                        //s'il n'existe pas on fait l'enregistrement
                                                    }
                                                }       
                                }
                        }
                }
        }
    }

include "menu.php";

?>
<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Enregistrement</title>
    </head>


<body>

<div class="container-fluid">

    <div class="row-fluid">

<!--  Création du formulaire d'enregistrement  -->
<form method="post" action="" id="formRegister">
    <fieldset>
        <legend>Enregistrement</legend>
            <div class="control-group">
                <div class="controls"> 

                <p><input type="text" name="name" autocomplete="off" placeholder="Prénom"/><br/></p>
            
                <p><input type="text" name="lastname" autocomplete="off" placeholder="Nom"/><br/></p>

                <p><input type="text" name="username" autocomplete="off" placeholder="Pseudo"/><br/></p>

                <p><input type="text" name="address" autocomplete="off" placeholder="Adresse"/><br/></p>

                <p><input type="text" name="phone" autocomplete="off" placeholder="Numéro de téléphone"/><br/></p>
            
                <p><input type="password" name="password" autocomplete="off" placeholder="Mot de passe"/><br/></p> 
            
                <p><input type="password" name="confirmation" autocomplete="off" placeholder="Confirmer Mot de passe"/><br/></p>

                <p>Type de compte: <br><br>
                    <select name="type" Id="liste"  onChange="Lien()">
                    <option value="1">Utilisateur</option>
                    <option value="2">Propriétaire</option>
                </select></p>   
     
                <p><button type="submit" class="btn btn-success pull-left">S'enregistrer <i class="icon-white icon-ok-sign"></i></button>
                <a class="lien1" href="login.php">&nbsp Deja enregistre ?</a></p>
            
            <!--  séquence d'affichage des massages d'erreur avec bootstrap  -->
                <?php if ($var == 1): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Votre pseudo est trop court</div>
                
                <?php elseif ($var == 2): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Votre mot de passe est trop court</div>
                
                <?php elseif ($var == 3): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Les mots de passe ne correspondent pas</div>
                
                <?php elseif ($var == 4): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    L'utilisateur existe deja</div>

                <?php elseif ($var == 5): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Le numéro de téléphone fournis est invalide</div>

                <?php elseif ($var == 6): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    L'adresse entrée n'est pas bonne</div>

                <?php elseif ($var == 7): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Ce pseudo est déjà utilisé</div>

                <?php elseif ($var == 8): ?>
                  
                    <div class="alert alert-success">
                    <h4 class="alert-heading">Félicitation</h4>
                    Votre compte a bien été créé</div>
                    <a href='login.php'>Se connecter</a>    
                <?php endif; ?>
                
            </div>
        </div>
        </table>
    </fieldset>
</form>
</div>
</div>
</body>
</html>
