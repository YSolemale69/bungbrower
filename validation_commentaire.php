<?php
session_start();

if (isset($_SESSION['user']['name'])):?>   

<?php else: header("Location: index.php");?>

<?php endif; ?>

<!DOCTYPE html >
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Valider commentaire</title>
    </head>


    <body>  

        <?php include"menu.php"; ?>

        <div id="main">

            <div class="container-fluid"> 

                <div class="row-fluid">
                    <div class="span12">

                        <?php 
                        include "classes/commentaire.class.php"; 
                        $new_member= new Commentaire;
                        
                        if (isset($_GET['test']) && $_GET['test'] == 1)
                            { $new_member->Validate(); }

                        elseif (isset($_GET['test']) && $_GET['test'] == 0)
                            { $new_member->Delete(); }
                        ?>

                    </div>

                </div>
            </div>
        </div>      
    </body>
</html>