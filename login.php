<?php
session_start();
$_SESSION['var'] = 1;

if (isset($_POST['username']) && isset($_POST['password'])) 
{
    include ("classes/login.class.php");

    $new_login = new Login;

    $new_login->preparation();

    $new_login->verification();
}

include "menu.php";

?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Connexion</title>
    </head>


<body>
<form method="post" action="" id="formLogin">
    <fieldset>
        <legend>Connexion</legend>

                 <div class="control-group">

                    <div class="controls">  

                        <p><input type="text" name="username" autocomplete="off" placeholder="Nom d'utilisateur" class="inputText"/><br/></p>


                        <p><input type="password" name="password" autocomplete="off" placeholder="Mot de passe" class="inputText"/><br/></p>

                        <p>
                            <button type="submit" class="btn btn-success pull-left">Se connecter <i class="icon-white icon-ok-sign"></i></button>
                            <a class="lien1" href="register.php">&nbsp S'enregistrer</a>
                        </p>

                <!-- affichage du message d'erreur -->

                <?php if ($_SESSION['var'] == 0): ?>
                  
                        <div class="alert alert-error">
                            <h4 class="alert-heading">Erreur !</h4>
                            Le mot de passe ou le pseudo n'est pas bon pas la pein d'essayer de tricher!
                        </div>

                <?php endif; ?>

                    </div> 
                </div>

    </fieldset>

</form>
</body>
</html>