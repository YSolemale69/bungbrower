<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8" />
            <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
            <link rel="stylesheet" href="css/main.css" type="text/css" />
    </head>

      <body>

      <div id="container-fluid">
        <div class="row-fluid"> 
    
          <?php if (isset($_SESSION['user']['name']) && $_SESSION['type'] == 2):?>
            <!--  Menu à afficher si connecté et propriétaire  -->

            <nav class="navbar navbar-inverse">
              <div class="navbar-inner">
                <div class="container">
                  <ul class="nav"> 
                      <li> <a class="brand" href="index.php">Bungbrowser</a> </li>
                    <li class="divider-vertical"></li>

                    <li> <a href="recherche.php">Rechercher un Bungalow</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="dashboard.php">Ajouter un Bungalow</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="manager.php">Gérer ses Bungalows</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="about.php">A propos</a> </li>
                    <li class="divider-vertical"></li>
                  </ul>

                <form class="navbar-search pull-right" method="post" action="recherche.php">
                  <input class="search-query" type="text" name="search" placeholder="Recherche">
                </form>
                </div>
              </div>
            </nav>

            <div class="row"> 
              <div class="span4">
                Bonjour, <?php echo $_SESSION['user']['name'];?>
                <a class="lien2" href="logout.php">Se deconnecter</a>
              </div>
            </div>

          <?php elseif (isset($_SESSION['user']['name']) && $_SESSION['type'] == 1):?>
            <!--  Menu à afficher si connecté et non propriétaire  -->

            <nav class="navbar navbar-inverse">
              <div class="navbar-inner">
                <div class="container">
                  <ul class="nav"> 
                      <li> <a class="brand" href="index.php">Bungbrowser</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="recherche.php">Rechercher un Bungalow</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="about.php">A propos</a> </li>
                    <li class="divider-vertical"></li>
                  </ul>

                <form class="navbar-search pull-right" method="post" action="recherche.php">
                  <input class="search-query" type="text" name="search" placeholder="Recherche">
                </form>
                </div>
              </div>
            </nav>

            <div class="row"> 
              <div class="span4">
                Bonjour, <?php echo $_SESSION['user']['name'];?>
                <a class="lien2" href="logout.php">Se deconnecter</a>
              </div>
            </div>            

          <?php else: ?>
            <!--  Menu à afficher si non connecté   -->

            <nav class="navbar navbar-inverse">
              <div class="navbar-inner">
                <div class="container">
                  <ul class="nav"> 
                      <li> <a class="brand" href="index.php">Bungbrowser</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="login.php">Se connecter</a> </li>
                    <li class="divider-vertical"></li>

                      <li> <a href="register.php">Creer un compte</a> </li>
                    <li class="divider-vertical"></li>

                    <li> <a href="about.php">A propos</a> </li>
                    <li class="divider-vertical"></li>
                  </ul>
                  
                </div>
              </div>
            </nav>
 
          <?php endif; ?>

        </div>
       </div>
      </div>  
        
      </body>
</html>