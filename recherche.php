<?php
session_start();
ini_set('display_errors', true);

if (isset($_SESSION['user']['name'])):?>   

<?php else: header("Location: index.php");?>

<?php endif; ?>

<?php
$var = 0;
$prout = 0;
//On initialise la variable de vérification

if (isset($_POST['search'])) 
    {   
        $str = $_POST['search'];
        $str = strlen($str);
        //on récupère la taille de données entrées dans le formulaire

        if ($str < 1)
        {
            $prout = 1;
        }

        else
        {  
            $var = 1;
        }
    }

include "menu.php";

?>
<!DOCTYPE html >
<html>
    <head>
      <meta charset="utf-8" />
        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">    
        <link rel="stylesheet" href="css/main.css" type="text/css" />
        <link rel="icon" type="image/png" href="img/favicon.png" />
      <title>Recherche</title>
    </head>


    <body>  

    <div id="main">

      <div class="container-fluid">
        <div class="page-header"> <h2 style='text-align: center'>Recherche</h2> </div>  

        <div class="row-fluid">
            <div class="span12">

            <br>
            <form method="post" action="" id="formSearch"> 
                <input type="text" class="input-xxlarge" name="search" autocomplete="off" placeholder="Recherche..."/>
                <button type="submit" class="btn btn-success" style="margin-bottom:10px"> Rechercher <i class="icon-white icon-search"></i></button>
            </form>

                <?php if ($var == 1): 

                  include "classes/recherche.class.php";

                  $new_search = new Search;

                  $new_search->afficheSearch(); ?>

                <?php endif; ?>


              <!--  séquence d'affichage des messages d'erreur avec bootstrap  -->

                <?php if ($prout == 1): ?>
                  
                    <div class="alert alert-error">
                    <h4 class="alert-heading">Erreur !</h4>
                    Veuillez entrer un mot clé</div>
   
                <?php endif; ?>

            </div>

        </div>
      </div> 
    </div>     
    </body>
</html>